package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	private DBManager() {
		try {
			Class.forName("org.mysql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static Connection getConnection() {
		Properties properties = new Properties();
		try {
			InputStream in = new FileInputStream("app.properties");
			properties.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Connection connection = null;
		String connectionURL = properties.getProperty("connection.url");
		String username = properties.getProperty("username");
		String password = properties.getProperty("password");
		try {
			connection = DriverManager.getConnection(connectionURL, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList = new ArrayList<>();
		try (Connection conn = DBManager.getConnection()){
			Statement statement = conn.createStatement();
			String sql = "SELECT * FROM users";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				String tempLogin = resultSet.getString("login");
				User user = User.createUser(tempLogin);
				user.setId(resultSet.getInt("id"));
				userList.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("Failed to find users", e.getCause());
		}
		return userList;
	}

	public void insertUser(User user) throws DBException {
		if (user == null) return;
		String userLogin = user.getLogin();
		try (Connection conn = DBManager.getConnection()){
			String sql = "INSERT INTO users(login) values (?)";
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, userLogin);
			int userID = ps.executeUpdate();
			if (userID == 0) return;
			ResultSet resultSet = ps.getGeneratedKeys();
			if (resultSet.next())
				user.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			throw new DBException("Failed to insert user" + userLogin, e.getCause());
		}
	}

	public void deleteUsers(User... users) throws DBException {
		if (users.length == 0) return;
		try(Connection conn = DBManager.getConnection()) {
			String sql = "DELETE FROM users WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			for (User user : users) {
				ps.setInt(1, user.getId());
				ps.executeUpdate();
			}
			String sql2 = "DELETE FROM users_teams WHERE user_id = ?";
			ps = conn.prepareStatement(sql2);
			for (User user : users) {
				ps.setInt(1, user.getId());
				ps.executeUpdate();
			}
			ps.close();
		} catch (SQLException e) {
			throw new DBException("Failed to delete users", e.getCause());
		}
	}

	public User getUser(String login) throws DBException {
		User user;
		try(Connection conn = DBManager.getConnection()) {
			Statement statement = conn.createStatement();
			String sql = "SELECT * FROM users WHERE login=" + "'" + login + "'";
			ResultSet resultSet = statement.executeQuery(sql);
			resultSet.next();
			int userID = resultSet.getInt("id");
			user = User.createUser(login);
			user.setId(userID);
		} catch (SQLException e) {
			throw new DBException("Failed to get user" + login, e.getCause());
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team;
		try(Connection conn = DBManager.getConnection()) {
			Statement statement = conn.createStatement();
			String sql = "SELECT * FROM teams WHERE name=" + "'" + name + "'";
			ResultSet resultSet = statement.executeQuery(sql);
			resultSet.next();
			int teamID = resultSet.getInt("id");
			team = Team.createTeam(name);
			team.setId(teamID);
		} catch (SQLException e) {
			throw new DBException("Failed to get team" + name, e.getCause());
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList = new ArrayList<>();
		try (Connection conn = DBManager.getConnection()){
			Statement statement = conn.createStatement();
			String sql = "SELECT name FROM teams";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				String teamName = resultSet.getString(1);
				teamList.add(Team.createTeam(teamName));
			}
		} catch (SQLException e) {
			throw new DBException("Failed to find teams", e.getCause());
		}
		return teamList;
	}

	public void insertTeam(Team team) throws DBException {
		try(Connection conn = DBManager.getConnection()) {
			String teamName = team.getName();
			String sql = "INSERT INTO teams(name) values (?)";
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, teamName);
			int teamID = ps.executeUpdate();
			if (teamID == 0) return;
			ResultSet resultSet = ps.getGeneratedKeys();
			if (resultSet.next()) team.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			throw new DBException("Failed to insert team", e.getCause());
		}
	}

	public void setTeamsForUser(User user, Team... teams) throws DBException {
		if (teams.length == 0) return;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			conn.setAutoCommit(false);
			int userID = user.getId();
			int teamID;
			String sql = "INSERT INTO users_teams(user_id, team_id) values (?, ?)";
			for (Team team : teams) {
				ps = conn.prepareStatement(sql);
				teamID = team.getId();
				ps.setInt(1, userID);
				ps.setInt(2, teamID);
				ps.executeUpdate();
			}
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("Teams for User fail and rollback", e.getCause());
		} finally {
			if (conn != null && ps != null) {
				try {
					ps.close();
					conn.setAutoCommit(true);
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teamList = new ArrayList<>();
		List<Integer> teamsIDList = new ArrayList<>();
		Team currentTeam;
		int userID = user.getId();
		try (Connection conn = DBManager.getConnection()){
			PreparedStatement findTeamsPS = conn.prepareStatement("SELECT * FROM users_teams WHERE user_id=?");
			findTeamsPS.setInt(1, userID);
			ResultSet resultSet = findTeamsPS.executeQuery();
			while (resultSet.next()) {
				teamsIDList.add(resultSet.getInt("team_id"));
			}
			for (int teamID : teamsIDList) {
				findTeamsPS = conn.prepareStatement("SELECT * FROM teams WHERE id=?");
				findTeamsPS.setInt(1, teamID);
				ResultSet rs = findTeamsPS.executeQuery();
				while (rs.next()) {
					currentTeam = Team.createTeam(rs.getString("name"));
					currentTeam.setId(teamID);
					teamList.add(currentTeam);
				}
			}
		} catch (SQLException e) {
			throw new DBException("Failed to get user teams", e.getCause());
		}
		return teamList;
	}

	public void deleteTeam(Team team) throws DBException {
		try(Connection conn = DBManager.getConnection()) {
			String sql = "DELETE FROM teams WHERE id = ?";
			String sql2 = "DELETE FROM users_teams WHERE team_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, team.getId());
			ps.executeUpdate();

			ps = conn.prepareStatement(sql2);
			ps.setInt(1, team.getId());
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			throw new DBException("Failed to delete team", e.getCause());
		}
	}

	public void updateTeam(Team team) throws DBException {
		try(Connection conn = DBManager.getConnection()) {
			int teamID = team.getId();
			String teamName = team.getName();
			String sql = "UPDATE teams SET name = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, teamName);
			ps.setInt(2, teamID);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			throw new DBException("Failed to update team", e.getCause());
		}
	}
}
